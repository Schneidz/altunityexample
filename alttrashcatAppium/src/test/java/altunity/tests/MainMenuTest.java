package altunity.tests;

import alttrashcatj.configreader.PropFileReader;
import alttrashcatj.pages.MainMenuPage;
import alttrashcatj.pages.StartPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.junit.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import ro.altom.altunitytester.AltUnityDriver;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class MainMenuTest {

    private static AltUnityDriver driver;
    private static AppiumDriver appiumDriver;
    private static StartPage startPage;
    private static MainMenuPage mainMenuPage;

    @BeforeClass
    public static void setUp() throws IOException {
        String deviceName ="";
        String app = "/alttrashcat_latest.apk";
        String url ="";
        System.out.println("*** AltUnity - local host");
        deviceName = "localhost:7010";
        //deviceName = "2271469230027ece";

        app = "\\Users\\aaron.schneider\\Work\\alttrashcatAppium\\alttrashcatAppium\\application.apk";
        url = "http://localhost:4723/wd/hub";
        AltUnityDriver.setupPortForwarding("android", "", 13000, 13000);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", deviceName);
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("automationName", "UiAutomator2");
        capabilities.setCapability("app", app);

        appiumDriver = new AndroidDriver(new URL(url), capabilities);
        System.out.println("*** AndroidDriver - was created " );
        appiumDriver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
        driver = new AltUnityDriver("127.0.0.1", 13000);
    }

    @Before
    public void loadLevel(){
        mainMenuPage = new MainMenuPage(driver);
        mainMenuPage.loadScene();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.stop();
        appiumDriver.quit();
        Thread.sleep(1000);
    }


    @Test
    public void TestMainPageLoadedCorrectly(){

        mainMenuPage.setCharacterName();
        mainMenuPage.setLeaderBoardButton();
        mainMenuPage.setMissionButton();
        mainMenuPage.setRunButton();
        mainMenuPage.setSettingsButton();
        mainMenuPage.setStoreButton();
        mainMenuPage.setThemeName();
        assertTrue(mainMenuPage.isDisplayed());
    }

}
