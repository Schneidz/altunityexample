package altunity.tests;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        StartPageTest.class,
        MainMenuTest.class,
        ShopTest.class,
        GamePlayTest.class,
})

public class TestSuite {
    // This class remains empty, it is used only as a holder for the above annotations
}
