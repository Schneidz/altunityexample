package altunity.tests;

import alttrashcatj.configreader.PropFileReader;
import alttrashcatj.pages.MainMenuPage;
import alttrashcatj.pages.ShopPage;
import alttrashcatj.pages.StartPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import ro.altom.altunitytester.AltUnityDriver;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ShopTest {

    private static AltUnityDriver driver;
    private static AppiumDriver appiumDriver;
    private static MainMenuPage mainMenuPage;
    private static ShopPage shopPage;

    private static void getAllObjectsShopPage(){
        shopPage.getStoreTitle();
        shopPage.getAccessoriesButton();
        shopPage.getCharactersButton();
        shopPage.getItemsButton();
        shopPage.getCloseButton();
        shopPage.getThemesButton();
        shopPage.getPremiumButton();
        shopPage.getCoinSection();
    }

    private static void getAllObjectsMainMenuPage(){
        mainMenuPage.setStoreButton();
        mainMenuPage.setThemeName();
        mainMenuPage.setSettingsButton();
        mainMenuPage.setRunButton();
        mainMenuPage.setMissionButton();
        mainMenuPage.setLeaderBoardButton();
        mainMenuPage.setCharacterName();
    }

    @BeforeClass
    public static void setUp() throws IOException {
        String deviceName ="";
        String app = "/alttrashcat_latest.apk";
        String url ="";
        System.out.println("*** AltUnity - local host");
        deviceName = "localhost:7005";
        //deviceName = "2271469230027ece";

        app = "\\Users\\aaron.schneider\\Work\\alttrashcatAppium\\alttrashcatAppium\\application.apk";
        url = "http://localhost:4723/wd/hub";
        AltUnityDriver.setupPortForwarding("android", "", 13000, 13000);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", deviceName);
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("automationName", "UiAutomator2");
        capabilities.setCapability("app", app);

        appiumDriver = new AndroidDriver(new URL(url), capabilities);
        System.out.println("*** AndroidDriver - was created " );
        appiumDriver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
        driver = new AltUnityDriver("127.0.0.1", 13000);
    }

    @Before
    public void loadLevel(){
        mainMenuPage = new MainMenuPage(driver);
        shopPage = new ShopPage(driver);
        shopPage.loadScene();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.stop();
        Thread.sleep(1000);
    }

    @Test
    public void ShopPageLoadedCorrectly(){
        getAllObjectsShopPage();
        assertTrue(shopPage.isDisplayedCorrectly());
        shopPage.pressClose();
    }

    @Test
    public void testShopPageCanBeClosed(){
        getAllObjectsShopPage();
        shopPage.pressClose();
        mainMenuPage.loadScene();
        getAllObjectsMainMenuPage();
        assertTrue(mainMenuPage.isDisplayed());
    }

    @Test
    public void testBuySomething(){
        getAllObjectsShopPage();
        shopPage.storeTitle.tap();
        int indexOfElement = 0;
        Integer beforeBuy = shopPage.getShopItemCount(indexOfElement);
        shopPage.clickBuyButton(0);
        Integer afterBuy = shopPage.getShopItemCount(indexOfElement);
        assertTrue(beforeBuy + 1 == afterBuy);

    }

    @Test
    public void testPremiumPopUpOpen(){
        shopPage.getPremiumButton();
        shopPage.pressPremiumPopUp();
        shopPage.getPopup();
        assertTrue(shopPage.checkPopupOpen());
    }

    @Test
    public void testPremiumPopUpClosed(){
        shopPage.getPremiumButton();
        shopPage.pressPremiumPopUp();
        shopPage.getPopup();
        shopPage.getClosePopupButton();
        shopPage.pressClosePremiumPopup();
        assertFalse(shopPage.checkPopupOpen());
    }
}
