package altunity.tests;

import alttrashcatj.configreader.PropFileReader;
import alttrashcatj.pages.*;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import ro.altom.altunitytester.AltUnityDriver;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class GamePlayTest {

    private static AltUnityDriver driver;
    private static AppiumDriver appiumDriver;
    private static MainMenuPage mainMenuPage;
    private static PauseOverlayPage pauseOverlayPage;
    private static GetAntoherChancePage getAntoherChancePage;
    private static GamePlayPage gamePlayPage;

    @BeforeClass
    public static void setUp() throws IOException {
        String deviceName ="";
        String app = "/alttrashcat_latest.apk";
        String url ="";
        System.out.println("*** AltUnity - local host");
        deviceName = "localhost:7005";
        //deviceName = "2271469230027ece";

        app = "\\Users\\aaron.schneider\\Work\\alttrashcatAppium\\alttrashcatAppium\\application.apk";
        url = "http://localhost:4723/wd/hub";
        AltUnityDriver.setupPortForwarding("android", "", 13000, 13000);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", deviceName);
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("automationName", "UiAutomator2");
        capabilities.setCapability("app", app);

        appiumDriver = new AndroidDriver(new URL(url), capabilities);
        System.out.println("*** AndroidDriver - was created " );
        appiumDriver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
        driver = new AltUnityDriver("127.0.0.1", 13000);
    }

    @Before
    public void loadLevel() throws Exception {

        mainMenuPage = new MainMenuPage(driver);
        mainMenuPage.loadScene();
        mainMenuPage.setCharacterName();
        mainMenuPage.setLeaderBoardButton();
        mainMenuPage.setMissionButton();
        mainMenuPage.setRunButton();
        mainMenuPage.setSettingsButton();
        mainMenuPage.setStoreButton();
        mainMenuPage.setThemeName();

        gamePlayPage = new GamePlayPage(driver);
        pauseOverlayPage = new PauseOverlayPage(driver);
        getAntoherChancePage = new GetAntoherChancePage(driver);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.stop();
        Thread.sleep(1000);
    }

    @Test
    public void testGamePlayDisplayedCorrectly(){
        mainMenuPage.pressRun();
        gamePlayPage.getPauseButton();
        gamePlayPage.getCharacter();
        assertTrue(gamePlayPage.isDisplayed());
    }

    @Test
    public void testGameCanBePausedAndResumed(){
        mainMenuPage.pressRun();
        gamePlayPage.getCharacter();
        gamePlayPage.getPauseButton();
        gamePlayPage.pressPause();
        pauseOverlayPage.getTitle();
        pauseOverlayPage.getMainMenuButton();
        pauseOverlayPage.getResumeButton();
        assertTrue(pauseOverlayPage.isDisplayed());
        pauseOverlayPage.pressResume();
        assertTrue(gamePlayPage.isDisplayed());
    }

    @Test
    public void testGameCanBePausedAndStopped(){
        mainMenuPage.pressRun();
        gamePlayPage.getCharacter();
        gamePlayPage.getPauseButton();
        gamePlayPage.pressPause();
        pauseOverlayPage.getTitle();
        pauseOverlayPage.getMainMenuButton();
        pauseOverlayPage.getResumeButton();
        pauseOverlayPage.pressMainMenu();
        assertTrue(mainMenuPage.isDisplayed());
    }

    @Test
    public void testAvoidingObstacles() throws Exception {
        mainMenuPage.pressRun();
        gamePlayPage.getCharacter();
        gamePlayPage.getPauseButton();
        gamePlayPage.avoidObstacles(10);
        assertTrue(gamePlayPage.getCurrentLife()>0);
    }

    @Test
    public void testPlayerDiesWhenObstacleNotAvoided() throws Exception {

        mainMenuPage.pressRun();
        gamePlayPage.getCharacter();
        gamePlayPage.getPauseButton();

        float timeout = 20;
        while(timeout>0){
            try {
                getAntoherChancePage.getGameOver();
                getAntoherChancePage.getAvailableCurrency();
                getAntoherChancePage.getPremiumButton();
                getAntoherChancePage.isDisplayed();
                break;
            }catch(Exception e){
                timeout -= 1;
            }
        }

        assertTrue(getAntoherChancePage.isDisplayed());

    }
}
